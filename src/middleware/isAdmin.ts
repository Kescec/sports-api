import { NextFunction, Request, Response } from 'express';
import { AuthorizationError } from '../error/errors';

export const isAdmin = async (req: Request, res: Response, next: NextFunction) => {
    const roles = req.user!.roles;

    if (!roles.map((value) => value.role).includes('admin')) {
        return next(new AuthorizationError('Invalid authorization level.'));
    }

    next();
};
