import { AnyZodObject } from 'zod';
import { NextFunction, Request, Response } from 'express';

export const validateQuery = (schema: AnyZodObject) => async (req: Request, res: Response, next: NextFunction) => {
    try {
        await schema.parseAsync(req.query);
        next();
    } catch (error) {
        next(error);
    }
};

export const validateBody = (schema: AnyZodObject) => async (req: Request, res: Response, next: NextFunction) => {
    try {
        await schema.parseAsync(req.body);
        next();
    } catch (error) {
        next(error);
    }
};
