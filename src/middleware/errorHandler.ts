import { NextFunction, Request, Response } from 'express';
import { ZodError } from 'zod';
import { AuthenticationError, AuthorizationError, InvalidActionError, NotFoundError } from '../error/errors';
import logger from '../util/logger';

export function errorHandler(err: Error, req: Request, res: Response, _: NextFunction) {
    logger.error(err);

    if (err instanceof ZodError) {
        respondToZodError(err, req, res);
    } else if (err instanceof InvalidActionError) {
        respondToInvalidActionError(err, req, res);
    } else if (err instanceof AuthenticationError) {
        respondToAuthenticationError(err, req, res);
    } else if (err instanceof AuthorizationError) {
        respondToAuthorizationError(err, req, res);
    } else if (err instanceof NotFoundError) {
        respondToNotFoundError(err, req, res);
    } else {
        respondToError(err, req, res);
    }
}

function respondToZodError(err: ZodError, req: Request, res: Response) {
    res.status(400);
    res.send({ message: err.errors });
}

function respondToInvalidActionError(err: Error, req: Request, res: Response) {
    res.status(400);
    res.send({ message: err.message });
}

function respondToAuthenticationError(err: Error, req: Request, res: Response) {
    res.status(401);
    res.send({ message: err.message });
}

function respondToAuthorizationError(err: Error, req: Request, res: Response) {
    res.status(403);
    res.send({ message: err.message });
}

function respondToNotFoundError(err: Error, req: Request, res: Response) {
    res.status(404);
    res.send({ message: err.message });
}

function respondToError(err: Error, req: Request, res: Response) {
    res.status(500);
    res.send({ message: err });
}
