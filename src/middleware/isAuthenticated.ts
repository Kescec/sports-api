import { NextFunction, Request, Response } from 'express';
import { AuthenticationError, NotFoundError } from '../error/errors';
import { verifyJwt } from '../util/jwt';
import { findUserById } from '../service/user.query.service';
import { JwtPayload } from 'jsonwebtoken';

export const isAuthenticated = async (req: Request, res: Response, next: NextFunction) => {
    const jwt: string | undefined = req.cookies.token;

    if (!jwt) {
        return next(new AuthenticationError('Invalid credentials.'));
    }

    let jwtPayload: JwtPayload;
    try {
        jwtPayload = verifyJwt(jwt) as JwtPayload;
    } catch (e: any) {
        const message = e instanceof NotFoundError ? 'Invalid credentials' : e.message;

        return next(new AuthenticationError(message));
    }

    req.user = await findUserById(Number(jwtPayload.sub));

    next();
};
