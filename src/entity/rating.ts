import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SportClass } from './sport.class';

@Entity()
export class Rating {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    grade!: number;

    @Column()
    comment!: string;

    @ManyToOne(() => SportClass, (sportClass) => sportClass.ratings, { onDelete: 'CASCADE', orphanedRowAction: 'delete' })
    class!: SportClass;
}
