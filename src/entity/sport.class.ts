import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Sport } from './sport';
import { Term } from './term';
import { Rating } from './rating';

@Entity()
export class SportClass {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    description!: string;

    @ManyToOne(() => Sport)
    sport!: Sport;

    @OneToMany(() => Term, (term) => term.sportClass, {
        cascade: true,
    })
    terms!: Term[];

    @Column({ type: 'numeric', default: 0 })
    average!: number;

    @OneToMany(() => Rating, (rating) => rating.class, { cascade: true })
    ratings!: Rating[];
}
