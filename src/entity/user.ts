import { Column, Entity, JoinColumn, ManyToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Term } from './term';
import { Verification } from './verification';
import { Role } from './role';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        unique: true,
    })
    email!: string;

    @Column()
    hash!: string;

    @ManyToMany(() => Role, (role) => role.users)
    roles!: Role[];

    @ManyToMany(() => Term, (term) => term.users)
    enrolled!: Term[];

    @OneToOne(() => Verification, { cascade: true, onDelete: 'CASCADE' })
    @JoinColumn()
    verification!: Verification;
}
