import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user';
import { SportClass } from './sport.class';
import { Age } from './age';

@Entity()
export class Term {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    start!: Date;

    @Column()
    end!: Date;

    @ManyToOne(() => SportClass, (sportClass) => sportClass.terms, {
        onDelete: 'CASCADE',
        orphanedRowAction: 'delete',
    })
    sportClass!: SportClass;

    @ManyToOne(() => Age)
    age!: Age;

    @ManyToMany(() => User, (user) => user.enrolled)
    @JoinTable()
    users!: User[];
}
