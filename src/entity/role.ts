import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user';

@Entity()
export class Role {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    role: string;

    @ManyToMany(() => User, (user) => user.roles)
    @JoinTable()
    users!: User[];

    constructor(role: string) {
        this.role = role;
    }
}
