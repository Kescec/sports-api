import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Verification {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ unique: true })
    verificationHash!: string;

    @Column()
    verified!: boolean;
}
