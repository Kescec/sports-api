import { PostgreDatasource } from '../config/postgre.datasource';
import { Term } from '../entity/term';
import { NotFoundError } from '../error/errors';

const termRepository = PostgreDatasource.getRepository(Term);

export async function findTermById(id: number) {
    const result = await termRepository.findOne({
        where: {
            id,
        },
        relations: {
            users: true,
        },
    });

    if (!result) {
        throw new NotFoundError('Term with id ' + id + ' not found');
    }

    return result;
}
