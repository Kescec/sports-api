import { PostgreDatasource } from '../config/postgre.datasource';
import { Sport } from '../entity/sport';
import { NotFoundError } from '../error/errors';

const sportRepository = PostgreDatasource.getRepository(Sport);

export async function findSportById(id: number) {
    const result = await sportRepository.findOne({
        where: {
            id,
        },
    });

    if (!result) {
        throw new NotFoundError('Sport with id ' + id + ' not found');
    }

    return result;
}
