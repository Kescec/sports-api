import { PostgreDatasource } from '../config/postgre.datasource';
import { SportClass } from '../entity/sport.class';
import { FindSportClassQueryType } from './query/findSportClassQuery';
import { NotFoundError } from '../error/errors';

const classRepository = PostgreDatasource.getRepository(SportClass);

export async function findSportClassById(id: number) {
    const result = await getSportClassQueryBuilder().where('class.id = :id', { id }).getOne();

    if (!result) {
        throw new NotFoundError('Sport class with id ' + id + ' not found');
    }

    return result;
}

export async function findSportClassByQuery(findClassQuery: FindSportClassQueryType) {
    const selectQueryBuilder = getSportClassQueryBuilder();

    if (findClassQuery.sports?.length) {
        selectQueryBuilder.andWhere('sport.name IN (:...sports)', {
            sports: findClassQuery.sports,
        });
    }

    if (findClassQuery.age?.length) {
        selectQueryBuilder.andWhere('age.name IN (:...age)', {
            age: findClassQuery.age,
        });
    }

    if (findClassQuery.description) {
        selectQueryBuilder.andWhere('description LIKE :description', {
            description: `%${findClassQuery.description}%`,
        });
    }

    return await selectQueryBuilder.getMany();
}

function getSportClassQueryBuilder() {
    return classRepository
        .createQueryBuilder('class')
        .leftJoinAndSelect('class.sport', 'sport')
        .leftJoinAndSelect('class.terms', 'terms')
        .leftJoinAndSelect('terms.age', 'age')
        .leftJoinAndSelect('class.ratings', 'ratings');
}
