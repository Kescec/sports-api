import { PostgreDatasource } from '../config/postgre.datasource';
import { NotFoundError } from '../error/errors';
import { Role } from '../entity/role';

const roleRepository = PostgreDatasource.getRepository(Role);

export async function findRoleById(id: number) {
    const result = await roleRepository.findOne({
        where: {
            id,
        },
    });

    if (!result) {
        throw new NotFoundError('Role with id ' + id + ' not found');
    }

    return result;
}
