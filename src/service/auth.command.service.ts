import { PostgreDatasource } from '../config/postgre.datasource';
import { User } from '../entity/user';
import { RegisterUserCommandType } from './dto/registerUserCommand';
import { checkHash, encryptPassword } from '../util/crypt';
import { Verification } from '../entity/verification';
import { randomUUID } from 'crypto';
import { sendMail } from './mail.service';
import { LoginUserCommandType } from './dto/loginUserCommand';
import { findUserByEmail } from './user.query.service';
import { createJwt } from '../util/jwt';
import { getServerAddress } from '../util/address';
import { AuthenticationError } from '../error/errors';

const userRepository = PostgreDatasource.getRepository(User);
const verificationRepository = PostgreDatasource.getRepository(Verification);

export async function register(registerUserCommand: RegisterUserCommandType) {
    const verification = new Verification();
    verification.verificationHash = randomUUID();
    verification.verified = false;

    const user = new User();
    user.email = registerUserCommand.email;
    user.hash = await encryptPassword(registerUserCommand.password);
    user.verification = verification;

    const verificationMail = await sendMail({
        to: 'example@mail.com', // list of receivers
        subject: 'Verification mail', // Subject line
        html: getVerificationMail(verification.verificationHash), // html body})
    });
    await userRepository.save(user);

    return verificationMail;
}

export async function verify(verificationHash: string) {
    const verification = await verificationRepository.findOne({
        where: {
            verificationHash,
        },
    });

    if (!verification || verification.verified) {
        throw new AuthenticationError('User verification is invalid!');
    }

    verification.verified = true;
    await verificationRepository.save(verification);

    return verification.verified;
}

export async function login(loginUserCommand: LoginUserCommandType) {
    const user = await findUserByEmail(loginUserCommand.email);

    if (!user.verification.verified) {
        throw new AuthenticationError('Credentials are not correct!');
    }

    const correctPassword = await checkHash(loginUserCommand.password, user.hash);

    if (!correctPassword) {
        throw new AuthenticationError('Credentials are not correct!');
    }

    return createJwt(user.id);
}

function getVerificationMail(verificationHash: string) {
    return `
  <h1>Hello!</h1>
  <h2>Please verify your registration by clicking on supplied link bellow.</h2>
  <a href="${getServerAddress()}/auth/verify?verificationHash=${verificationHash}">Verify here!</a>
`;
}
