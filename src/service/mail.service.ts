import nodemailer from 'nodemailer';
import logger from '../util/logger';
import Mail from 'nodemailer/lib/mailer';

let nodemailerInstance: { account: nodemailer.TestAccount; transporter: nodemailer.Transporter } | undefined = undefined;

async function getInstance() {
    if (nodemailerInstance) {
        return nodemailerInstance;
    }

    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    const testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });

    nodemailerInstance = { account: testAccount, transporter };

    return nodemailerInstance;
}

type MailBody = Pick<Mail.Options, 'to' | 'subject' | 'text' | 'html'>;

export async function sendMail(mailBody: MailBody) {
    const nodemailerInstance = await getInstance();

    // send mail with defined transport object
    const info = await nodemailerInstance.transporter.sendMail({
        from: '"Sports api" <sports-api@mail.com>', // sender address
        ...mailBody,
    });

    logger.info('Message sent: %s', info.messageId);
    logger.info('Preview URL: %s', nodemailer.getTestMessageUrl(info));

    return nodemailer.getTestMessageUrl(info);
}
