import { PostgreDatasource } from '../config/postgre.datasource';
import { CreateUserCommandType } from './dto/createUserCommand';
import { findUserByEmail, findUserById, userExistsByEmail } from './user.query.service';
import { InvalidActionError } from '../error/errors';
import { User } from '../entity/user';
import { encryptPassword } from '../util/crypt';
import { findRoleById } from './role.query.service';
import { UpdateUserCommandType } from './dto/updateUserCommand';

const userRepository = PostgreDatasource.getRepository(User);

export async function createUser(createUserCommand: CreateUserCommandType) {
    if (await userExistsByEmail(createUserCommand.email)) {
        throw new InvalidActionError('User with email already exists!');
    }

    const user = new User();
    user.email = createUserCommand.email;
    user.hash = await encryptPassword(createUserCommand.password);
    user.roles = await Promise.all(createUserCommand.roles.map(findRoleById));

    return await userRepository.save(user);
}

export async function updateUser(updateUserCommand: UpdateUserCommandType) {
    const user = await findUserById(updateUserCommand.id);
    const anotherUser = await findUserByEmail(updateUserCommand.email);

    if (user.id !== anotherUser.id) {
        throw new InvalidActionError('User with this email already exists!');
    }

    user.email = updateUserCommand.email;
    user.hash = await encryptPassword(updateUserCommand.password);
    user.roles = await Promise.all(updateUserCommand.roles.map(findRoleById));

    return await userRepository.save(user);
}

export async function deleteUser(userId: number) {
    const user = await findUserById(userId);

    await userRepository.remove(user);
}
