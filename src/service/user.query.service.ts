import { PostgreDatasource } from '../config/postgre.datasource';
import { User } from '../entity/user';
import { NotFoundError } from '../error/errors';

const userRepository = PostgreDatasource.getRepository(User);

export async function findUserById(id: number) {
    const result = await userRepository
        .createQueryBuilder('user')
        .leftJoinAndSelect('user.enrolled', 'enrolled')
        .leftJoinAndSelect('user.roles', 'roles')
        .where('user.id = :id', { id })
        .getOne();

    if (!result) {
        throw new NotFoundError('User with id ' + id + ' not found');
    }

    return result;
}

export async function findUserByEmail(email: string) {
    const result = await userRepository
        .createQueryBuilder('user')
        .leftJoinAndSelect('user.verification', 'verification')
        .where('user.email = :email', { email })
        .getOne();

    if (!result) {
        throw new NotFoundError('User with id ' + email + ' not found');
    }

    return result;
}

export async function userExistsByEmail(email: string) {
    return await userRepository.createQueryBuilder('user').where('user.email = :email', { email }).getExists();
}
