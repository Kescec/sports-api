export type FindSportClassQueryType = {
    description?: string | null;
    sports?: string[];
    age?: string[];
};
