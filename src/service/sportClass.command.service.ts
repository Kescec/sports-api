import { PostgreDatasource } from '../config/postgre.datasource';
import { SportClass } from '../entity/sport.class';
import { findSportClassById } from './sportClass.query.service';
import { Rating } from '../entity/rating';
import { CreateSportClassCommandType } from './dto/createSportClassCommand';
import { findSportById } from './sport.query.service';
import { TermCommandType } from './dto/termCommand';
import { Term } from '../entity/term';
import { findAgeById } from './age.query.service';
import { UpdateSportClassCommandType } from './dto/updateSportClassCommand';
import { RateSportClassType } from './dto/rateSportClassCommand';

const classRepository = PostgreDatasource.getRepository(SportClass);

export async function createSportClass(createSportClassCommand: CreateSportClassCommandType) {
    const sport = await findSportById(createSportClassCommand.sport);
    const sportClass = new SportClass();

    sportClass.description = createSportClassCommand.description;
    sportClass.terms = await Promise.all(createSportClassCommand.terms.map(mapTerm));
    sportClass.sport = sport;

    return await classRepository.save(sportClass);
}

export async function updateSportClass(updateSportClassCommand: UpdateSportClassCommandType) {
    const sportClass = await findSportClassById(updateSportClassCommand.id);
    const sport = await findSportById(updateSportClassCommand.sport);

    sportClass.description = updateSportClassCommand.description;
    sportClass.terms = await Promise.all(updateSportClassCommand.terms.map(mapTerm));
    sportClass.sport = sport;

    return await classRepository.save(sportClass);
}

export async function deleteSportClass(sportClassId: number) {
    const sportClass = await findSportClassById(sportClassId);

    await classRepository.remove(sportClass);
}

export async function rate(rateSportClassCommand: RateSportClassType) {
    const sportClass = await findSportClassById(rateSportClassCommand.sportClassId);

    const rating = new Rating();
    rating.grade = rateSportClassCommand.grade;
    rating.comment = rateSportClassCommand.comment;

    sportClass.average = (sportClass.average * sportClass.ratings.length + rating.grade) / (sportClass.ratings.length + 1);
    sportClass.ratings.push(rating);

    await classRepository.save(sportClass);
}

async function mapTerm(termCommand: TermCommandType) {
    const age = await findAgeById(termCommand.age);
    const term = new Term();

    term.start = new Date(termCommand.start);
    term.end = new Date(termCommand.end);
    term.age = age;

    return term;
}
