import { PostgreDatasource } from '../config/postgre.datasource';
import { Age } from '../entity/age';
import { NotFoundError } from '../error/errors';

const ageRepository = PostgreDatasource.getRepository(Age);

export async function findAgeById(id: number) {
    const result = await ageRepository.findOne({
        where: {
            id,
        },
    });

    if (!result) {
        throw new NotFoundError('Age with id ' + id + ' not found');
    }

    return result;
}
