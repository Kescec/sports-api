import z from 'zod';

export const RateSportClassSchema = z.object({
    sportClassId: z.number(),
    grade: z.number().min(1).max(5),
    comment: z.string().trim().max(500),
});

export type RateSportClassType = z.infer<typeof RateSportClassSchema>;
