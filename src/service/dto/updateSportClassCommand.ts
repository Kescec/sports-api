import z from 'zod';
import { CreateSportClassCommandSchema } from './createSportClassCommand';

export const UpdateSportClassCommandSchema = z
    .object({
        id: z.number(),
    })
    .merge(CreateSportClassCommandSchema);

export type UpdateSportClassCommandType = z.infer<typeof UpdateSportClassCommandSchema>;
