import z from 'zod';
import { CreateUserCommandSchema } from './createUserCommand';

export const UpdateUserCommandSchema = z
    .object({
        id: z.number(),
    })
    .merge(CreateUserCommandSchema);

export type UpdateUserCommandType = z.infer<typeof UpdateUserCommandSchema>;
