import z from 'zod';
import { RegisterUserCommandSchema } from './registerUserCommand';

export const LoginUserCommandSchema = RegisterUserCommandSchema;
export type LoginUserCommandType = z.infer<typeof RegisterUserCommandSchema>;
