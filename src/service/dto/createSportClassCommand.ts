import z from 'zod';
import { TermCommandSchema } from './termCommand';

export const CreateSportClassCommandSchema = z.object({
    description: z.string().trim().max(300),
    sport: z.number(),
    terms: z.array(TermCommandSchema),
});

export type CreateSportClassCommandType = z.infer<typeof CreateSportClassCommandSchema>;
