import z from 'zod';

export const TermCommandSchema = z.object({
    start: z.string().datetime(),
    end: z.string().datetime(),
    age: z.number(),
});

export type TermCommandType = z.infer<typeof TermCommandSchema>;
