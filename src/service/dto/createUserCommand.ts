import z from 'zod';
import { RegisterUserCommandSchema } from './registerUserCommand';

export const CreateUserCommandSchema = z.object({ roles: z.array(z.number()) }).merge(RegisterUserCommandSchema);

export type CreateUserCommandType = z.infer<typeof CreateUserCommandSchema>;
