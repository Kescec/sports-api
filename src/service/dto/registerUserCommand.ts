import z from 'zod';

export const RegisterUserCommandSchema = z.object({
    email: z.string().email().trim(),
    password: z.string().trim().min(8),
});

export type RegisterUserCommandType = z.infer<typeof RegisterUserCommandSchema>;
