import { PostgreDatasource } from '../config/postgre.datasource';
import { Term } from '../entity/term';
import { findTermById } from './term.query.service';
import { InvalidActionError } from '../error/errors';
import { User } from '../entity/user';

const termRepository = PostgreDatasource.getRepository(Term);

export async function enroll(termId: number, user: User) {
    if (user.enrolled.length === 2) {
        throw new InvalidActionError('User enrolled in maximum sport classes.');
    }

    const term = await findTermById(termId);

    if (term.users.length === 10) {
        throw new InvalidActionError('Maximum number of users enrolled.');
    }

    term.users.push(user);

    return await termRepository.save(term);
}

export async function unroll(termId: number, user: User) {
    if (!user.enrolled.find((value) => value.id === termId)) {
        throw new InvalidActionError('User not enrolled in given term.');
    }

    const term = await findTermById(termId);

    term.users = term.users.filter((value) => value.id !== user.id);

    return await termRepository.save(term);
}
