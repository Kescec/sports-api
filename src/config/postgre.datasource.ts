import 'reflect-metadata';

import { DataSource } from 'typeorm';
import { Age } from '../entity/age';
import { SportClass } from '../entity/sport.class';
import { Rating } from '../entity/rating';
import { Role } from '../entity/role';
import { Sport } from '../entity/sport';
import { Term } from '../entity/term';
import { User } from '../entity/user';
import { Verification } from '../entity/verification';

export const PostgreDatasource = new DataSource({
    type: 'postgres',
    host: `${process.env.DB_NAME || 'localhost'}`,
    port: 5432,
    username: `${process.env.DB_USER_NAME || 'postgres'}`,
    password: `${process.env.DB_PASSWORD || 'postgres'}`,
    database: 'sports',
    synchronize: true,
    logging: process.env.NODE_ENV !== 'production',
    entities: [Age, SportClass, Rating, Role, Sport, Term, User, Verification],
    subscribers: [],
    migrations: [],
});
