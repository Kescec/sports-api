import { Age } from '../entity/age';
import { PostgreDatasource } from './postgre.datasource';
import { Role } from '../entity/role';
import { Sport } from '../entity/sport';
import { Term } from '../entity/term';
import { User } from '../entity/user';
import { SportClass } from '../entity/sport.class';
import { Verification } from '../entity/verification';

export const insertInitialData = async () => {
    const user = new Role('user');
    const admin = new Role('admin');
    await PostgreDatasource.getRepository(Role).save([user, admin]);

    const user1 = new User();
    user1.email = 'email1@email.com';
    user1.hash = '$2a$12$RoI88GqyOBmm9eXg4Ulmqe6cTp.s9kID1HrH2h/K2L6/c3iz8TDii'; // password11
    user1.roles = [user];
    const verification1 = new Verification();
    verification1.verified = true;
    verification1.verificationHash = 'gna1';
    user1.verification = verification1;

    const user2 = new User();
    user2.email = 'email2@email.com';
    user2.hash = '$2a$12$laLyvXgSEx0LLVkXO7yzd.USrvNMxf0W4.nXkEpzzo7AokY0xZGfW'; // password2
    user2.roles = [admin];
    const verification2 = new Verification();
    verification2.verified = true;
    verification2.verificationHash = 'gna2';
    user2.verification = verification2;

    const user3 = new User();
    user3.email = 'email3@email.com';
    user3.hash = '$2a$12$laLyvXgSEx0LLVkXO7yzd.USrvNMxf0W4.nXkEpzzo7AokY0xZGfW'; // password3
    user3.roles = [user];
    const verification3 = new Verification();
    verification3.verified = true;
    verification3.verificationHash = 'gna3';
    user3.verification = verification3;

    const user4 = new User();
    user4.email = 'email4@email.com';
    user4.hash = '$2a$12$AZrLFjpDE.keAU6BKAUu9ulod1EHuDT11bbss6T0k9EABT7EUGbNG'; // password4
    user4.roles = [user];
    const verification4 = new Verification();
    verification4.verified = true;
    verification4.verificationHash = 'gna4';
    user4.verification = verification4;

    const user5 = new User();
    user5.email = 'email5@email.com';
    user5.hash = '$2a$12$tWzo9bhSaWluVncv4mnM3eJAuMivv3j310Zn2MmK0tgPtptw8iPKm'; // password5
    user5.roles = [user];
    const verification5 = new Verification();
    verification5.verified = true;
    verification5.verificationHash = 'gna5';
    user5.verification = verification5;
    await PostgreDatasource.getRepository(User).save([user1, user2, user3, user4, user5]);

    const age1 = new Age('children');
    const age2 = new Age('youth');
    const age3 = new Age('youngAdults');
    const age4 = new Age('adult');
    await PostgreDatasource.getRepository(Age).save([age1, age2, age3, age4]);

    const sport1 = new Sport('baseball');
    const sport2 = new Sport('basketball');
    const sport3 = new Sport('football');
    const sport4 = new Sport('boxing');
    const sport5 = new Sport('cycling');
    const sport6 = new Sport('fitness');
    const sport7 = new Sport('golf');
    const sport8 = new Sport('running');
    const sport9 = new Sport('swimming');
    const sport10 = new Sport('tennis');
    const sport11 = new Sport('triathlon');
    const sport12 = new Sport('volleyball');
    await PostgreDatasource.getRepository(Sport).save([
        sport1,
        sport2,
        sport3,
        sport4,
        sport5,
        sport6,
        sport7,
        sport8,
        sport9,
        sport10,
        sport11,
        sport12,
    ]);

    const term1 = new Term();
    term1.start = new Date(2021, 12, 20, 10);
    term1.end = new Date(2021, 12, 20, 11, 30);
    term1.age = age2;
    term1.users = [user1, user2];

    const term2 = new Term();
    term2.start = new Date(2022, 12, 21, 10);
    term2.end = new Date(2022, 12, 21, 11, 30);
    term2.age = age2;
    term2.users = [user3];

    const term3 = new Term();
    term3.start = new Date(2023, 12, 22, 10);
    term3.end = new Date(2023, 12, 22, 11, 30);
    term3.age = age3;
    term3.users = [user1];

    const sportClass1 = new SportClass();
    sportClass1.sport = sport2;
    sportClass1.terms = [term1];
    sportClass1.description = 'description1';

    const sportClass2 = new SportClass();
    sportClass2.sport = sport3;
    sportClass2.terms = [term2];
    sportClass2.description = 'description2';

    const sportClass3 = new SportClass();
    sportClass3.sport = sport2;
    sportClass3.terms = [term3];
    sportClass3.description = 'description3';

    await PostgreDatasource.getRepository(SportClass).save([sportClass1, sportClass2, sportClass3]);
};
