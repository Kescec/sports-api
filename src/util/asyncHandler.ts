import { NextFunction, Request, Response } from 'express';

type ThrowableAsyncFunction = (req: Request<any, any, any, any, any>, res: Response<any, any>, next?: NextFunction) => Promise<void>;

export const asyncHandler = (handler: ThrowableAsyncFunction) => async (req: Request, res: Response, next: NextFunction) => {
    try {
        await handler(req, res, next);
    } catch (e) {
        next(e);
    }
};
