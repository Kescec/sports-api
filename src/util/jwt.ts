import jwt from 'jsonwebtoken';

export const createJwt = (subject: number) => {
    return jwt.sign(
        {
            sub: subject,
            exp: Math.floor(Date.now() / 1000) + Number(process.env.JWT_LENGTH || 30),
            iss: 'sports-api',
        },
        process.env.SECRET_KEY || 'secret'
    );
};

export const verifyJwt = (token: string) => {
    return jwt.verify(token, process.env.SECRET_KEY || 'secret', { issuer: 'sports-api' });
};
