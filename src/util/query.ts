import { FindSportClassRequestType } from '../routes/request/findSportClassRequest';
import { FindSportClassQueryType } from '../service/query/findSportClassQuery';

export const prepareSportsClassQuery = (queryRequest: FindSportClassRequestType): FindSportClassQueryType => {
    const sports = queryRequest.sports?.split(',')?.map((value) => value.toLowerCase());
    const age = queryRequest.age?.split(',')?.map((value) => value.toLowerCase());

    return { ...queryRequest, sports, age };
};
