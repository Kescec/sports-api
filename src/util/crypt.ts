import bcrypt from 'bcrypt';

const saltRounds = Number(process.env.SALT_ROUNDS || 10);

export async function encryptPassword(password: string) {
    return await bcrypt.hash(password, saltRounds);
}

export async function checkHash(password: string, hash: string) {
    return await bcrypt.compare(password, hash);
}
