import ip from 'ip';

export const getServerAddress = () => {
    return `http://${process.env.NOVE_ENV === 'production' ? ip.address() : 'localhost:' + (process.env.PORT || 3000)}/api`;
};
