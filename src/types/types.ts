import { SportClass } from '../entity/sport.class';
import { Rating } from '../entity/rating';

export interface SportClassResponse extends Omit<SportClass, 'ratings'> {
    ratings?: Rating[];
}
