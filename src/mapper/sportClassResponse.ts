import { SportClass } from '../entity/sport.class';
import { User } from '../entity/user';
import { SportClassResponse } from '../types/types';

export const sportClassResponseBiMapper = (sportClass: SportClass, user: User): SportClassResponse => {
    const isAdmin = user.roles.map((value) => value.role).includes('admin');

    return { ...sportClass, ratings: isAdmin ? sportClass.ratings : undefined };
};
