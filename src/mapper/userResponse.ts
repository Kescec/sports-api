import { User } from '../entity/user';

export const userResponseMapper = (user: User): Partial<User> => {
    return { ...user, hash: undefined };
};
