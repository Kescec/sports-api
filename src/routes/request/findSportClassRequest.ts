import z from 'zod';

export const FindSportClassRequestSchema = z.object({
    description: z.string().trim().max(300).nullish(),
    sports: z.string().trim().nullish(),
    age: z.string().trim().nullish(),
});

export type FindSportClassRequestType = z.infer<typeof FindSportClassRequestSchema>;
