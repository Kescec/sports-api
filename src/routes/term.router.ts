import express, { Request } from 'express';
import { enroll, unroll } from '../service/term.command.service';
import { asyncHandler } from '../util/asyncHandler';

const termRouter = express.Router();

termRouter.post(
    '/:id/enroll',
    asyncHandler(async (req: Request<{ id: number }>, res) => {
        const id = Number(req.params.id);
        await enroll(id, req.user!);

        res.sendStatus(201);
    })
);

termRouter.post(
    '/:id/unroll',
    asyncHandler(async (req: Request<{ id: number }>, res) => {
        const id = Number(req.params.id);
        await unroll(id, req.user!);

        res.sendStatus(201);
    })
);

export default termRouter;
