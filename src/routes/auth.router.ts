import express, { Request } from 'express';
import { RegisterUserCommandSchema, RegisterUserCommandType } from '../service/dto/registerUserCommand';
import { validateBody, validateQuery } from '../middleware/validate';
import { login, register, verify } from '../service/auth.command.service';
import z from 'zod';
import { LoginUserCommandSchema, LoginUserCommandType } from '../service/dto/loginUserCommand';
import { asyncHandler } from '../util/asyncHandler';

const authRouter = express.Router();

authRouter.post(
    '/register',
    validateBody(RegisterUserCommandSchema),
    asyncHandler(async (req: Request<unknown, { verificationMail: string | false }, RegisterUserCommandType>, res) => {
        const registerUserCommand = req.body;
        const verificationMail = await register(registerUserCommand);

        res.send({ mail: verificationMail });
    })
);

authRouter.get(
    '/verify',
    validateQuery(z.object({ verificationHash: z.string().uuid() })),
    asyncHandler(async (req: Request<unknown, unknown, unknown, { verificationHash: string }>, res) => {
        const verificationHash = req.query.verificationHash;
        await verify(verificationHash);

        res.sendStatus(200);
    })
);

authRouter.post(
    '/login',
    validateBody(LoginUserCommandSchema),
    asyncHandler(async (req: Request<unknown, { jwt: string }, LoginUserCommandType>, res) => {
        const loginUserCommand = req.body;
        const jwt = await login(loginUserCommand);

        res.cookie('token', jwt, { httpOnly: true });
        res.sendStatus(200);
    })
);

export default authRouter;
