import express, { Request } from 'express';
import { SportClass } from '../entity/sport.class';
import { prepareSportsClassQuery } from '../util/query';
import { findSportClassById, findSportClassByQuery } from '../service/sportClass.query.service';
import { createSportClass, deleteSportClass, rate, updateSportClass } from '../service/sportClass.command.service';
import { validateBody, validateQuery } from '../middleware/validate';
import { CreateSportClassCommandSchema, CreateSportClassCommandType } from '../service/dto/createSportClassCommand';
import { UpdateSportClassCommandSchema, UpdateSportClassCommandType } from '../service/dto/updateSportClassCommand';
import { RateSportClassSchema, RateSportClassType } from '../service/dto/rateSportClassCommand';
import { FindSportClassRequestSchema, FindSportClassRequestType } from './request/findSportClassRequest';
import { asyncHandler } from '../util/asyncHandler';
import { isAdmin } from '../middleware/isAdmin';
import { InvalidActionError } from '../error/errors';
import { sportClassResponseBiMapper } from '../mapper/sportClassResponse';

const sportClassRouter = express.Router();

sportClassRouter.get(
    '/',
    validateQuery(FindSportClassRequestSchema),
    asyncHandler(async (req: Request<unknown, SportClass[], unknown, FindSportClassRequestType>, res) => {
        const query = prepareSportsClassQuery(req.query);
        const result = await findSportClassByQuery(query);

        res.send(result.map((value) => sportClassResponseBiMapper(value, req.user!)));
    })
);

sportClassRouter.get(
    '/:id',
    asyncHandler(async (req: Request<{ id: number }, SportClass>, res) => {
        const id = Number(req.params.id);
        const result = await findSportClassById(id);

        res.send(sportClassResponseBiMapper(result, req.user!));
    })
);

sportClassRouter.post(
    '/:id/rate',
    validateBody(RateSportClassSchema),
    asyncHandler(async (req: Request<{ id: number }, unknown, RateSportClassType, unknown>, res) => {
        const id = Number(req.params.id);
        const rateSportClassCommand = req.body;

        if (id !== rateSportClassCommand.sportClassId) {
            throw new InvalidActionError('Id mismatch!');
        }

        await rate(rateSportClassCommand);

        res.sendStatus(201);
    })
);

sportClassRouter.post(
    '/',
    isAdmin,
    validateBody(CreateSportClassCommandSchema),
    asyncHandler(async (req: Request<unknown, SportClass, CreateSportClassCommandType>, res) => {
        const createSportClassCommand = req.body;
        const result = await createSportClass(createSportClassCommand);

        res.send(result);
    })
);

sportClassRouter.put(
    '/:id',
    isAdmin,
    validateBody(UpdateSportClassCommandSchema),
    asyncHandler(async (req: Request<any, SportClass, UpdateSportClassCommandType>, res) => {
        const id = Number(req.params.id);
        const updateSportClassCommand = req.body;

        if (id !== updateSportClassCommand.id) {
            throw new InvalidActionError('Id mismatch!');
        }

        const result = await updateSportClass(updateSportClassCommand);

        res.send(result);
    })
);

sportClassRouter.delete(
    '/:id',
    isAdmin,
    asyncHandler(async (req: Request<{ id: number }>, res) => {
        const id = Number(req.params.id);
        await deleteSportClass(id);

        res.sendStatus(204);
    })
);

export default sportClassRouter;
