import express, { Request } from 'express';
import { validateBody } from '../middleware/validate';
import { UpdateUserCommandSchema, UpdateUserCommandType } from '../service/dto/updateUserCommand';
import { asyncHandler } from '../util/asyncHandler';
import { User } from '../entity/user';
import { findUserById } from '../service/user.query.service';
import { CreateUserCommandSchema, CreateUserCommandType } from '../service/dto/createUserCommand';
import { createUser, deleteUser, updateUser } from '../service/user.command.service';
import { InvalidActionError } from '../error/errors';
import { userResponseMapper } from '../mapper/userResponse';

const userRouter = express.Router();

userRouter.get(
    '/:id',
    asyncHandler(async (req: Request<{ id: number }, User>, res) => {
        const id = Number(req.params.id);
        const result = await findUserById(id);

        res.send(result);
    })
);

userRouter.post(
    '/',
    validateBody(CreateUserCommandSchema),
    asyncHandler(async (req: Request<unknown, User, CreateUserCommandType>, res) => {
        const createUserCommandType = req.body;
        const result = await createUser(createUserCommandType);

        res.send(userResponseMapper(result));
    })
);

userRouter.put(
    '/:id',
    validateBody(UpdateUserCommandSchema),
    asyncHandler(async (req: Request<any, User, UpdateUserCommandType>, res) => {
        const id = Number(req.params.id);
        const updateUserCommand = req.body;

        if (id !== updateUserCommand.id) {
            throw new InvalidActionError('Id mismatch!');
        }

        const result = await updateUser(updateUserCommand);

        res.send(userResponseMapper(result));
    })
);

userRouter.delete(
    '/:id',
    asyncHandler(async (req: Request<{ id: number }>, res) => {
        const id = Number(req.params.id);
        await deleteUser(id);

        res.sendStatus(204);
    })
);

export default userRouter;
