export class NotFoundError extends Error {}

export class AuthenticationError extends Error {}

export class AuthorizationError extends Error {}

export class InvalidActionError extends Error {}
