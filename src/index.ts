import express from 'express';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import logger from './util/logger';
import morganMiddleware from './middleware/morgan';
import sportClassRouter from './routes/sportClass.router';
import { PostgreDatasource } from './config/postgre.datasource';
import { insertInitialData } from './config/initialData';
import termRouter from './routes/term.router';
import authRouter from './routes/auth.router';
import { errorHandler } from './middleware/errorHandler';
import { isAuthenticated } from './middleware/isAuthenticated';
import userRouter from './routes/user.router';
import { isAdmin } from './middleware/isAdmin';

function startServer() {
    const app = express();

    app.use(cookieParser());
    app.use(morganMiddleware);
    app.use(express.json());

    app.use('/api/auth', authRouter);
    app.use('/api/classes', isAuthenticated, sportClassRouter);
    app.use('/api/terms', isAuthenticated, termRouter);
    app.use('/api/users', isAuthenticated, isAdmin, userRouter);

    app.use(errorHandler);

    const port = process.env.PORT || 3000;
    app.listen(port, () => {
        logger.info(`Listening on port ${port}!`);
    });
}

dotenv.config();

PostgreDatasource.initialize().then(insertInitialData).then(startServer).catch(logger.error);
